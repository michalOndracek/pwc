package com.pwc.route.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RouteException extends Exception {
    public RouteException(String message) {
        super(message);
    }
}
