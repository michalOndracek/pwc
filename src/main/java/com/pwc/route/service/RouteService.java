package com.pwc.route.service;

import com.pwc.route.exception.RouteException;
import com.pwc.route.model.Route;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

@Service
@AllArgsConstructor
public class RouteService {

    private final CountryDataHolder countryDataHolder;

    public Route getRoute(String origin, String destination) throws RouteException {
        if (isNotCca3Code(origin)) {
            throw new RouteException("Origin country code is not cca3");
        }
        if (isNotCca3Code(destination)) {
            throw new RouteException("Destination country code is not cca3");
        }

        return new Route(findRoute(origin, destination));
    }

    private List<String> findRoute(String originCode, String destinationCode) throws RouteException {
        Map<String, String> visitedCountries = new HashMap<>();
        Queue<String> queue = new LinkedList<>();
        queue.add(originCode);
        visitedCountries.put(originCode, null);

        while (!queue.isEmpty()) {
            String currentCode = queue.remove();
            if (currentCode.equals(destinationCode)) {
                return createPath(destinationCode, visitedCountries);
            }

            Set<String> currentCountryNeighbors =
                    countryDataHolder.getCountries().get(currentCode).getBorderingCountriesCodes();

            currentCountryNeighbors.stream()
                    .filter(neighborCode -> !visitedCountries.containsKey(neighborCode))
                    .forEach(neighborCode -> {
                        visitedCountries.put(neighborCode, currentCode);
                        queue.add(neighborCode);
                    });
            ;
        }
        throw new RouteException("There is no route between the countries");
    }

    private boolean isNotCca3Code(String countryCode) {
        return !countryDataHolder.getCountries().containsKey(countryCode);
    }

    private List<String> createPath(String destinationCode, Map<String, String> visitedCountries) {
        List<String> path = new ArrayList<>();
        String code = destinationCode;
        while (code != null) {
            path.add(code);
            code = visitedCountries.get(code);
        }
        Collections.reverse(path);
        return path;
    }
}
