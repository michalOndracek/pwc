package com.pwc.route.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pwc.route.JsonLoader;
import com.pwc.route.model.Country;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Getter
@AllArgsConstructor
public class CountryDataHolder {


    private Map<String, Country> countries;

    @PostConstruct
    public void loadJsonData() throws IOException {
        String loadedCountries = JsonLoader.loadJsonFile("countryData.json");
        ObjectMapper objectMapper = new ObjectMapper();
        List<Country> countryList = Arrays.asList(objectMapper.readValue(loadedCountries, Country[].class));
        countries = countryList.stream()
                        .filter(country -> !country.getCountryCode().isEmpty())
                        .collect(Collectors.toMap(Country::getCountryCode, Function.identity()));
    }
}
