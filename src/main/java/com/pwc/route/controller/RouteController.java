package com.pwc.route.controller;

import com.pwc.route.exception.RouteException;
import com.pwc.route.model.Route;
import com.pwc.route.service.RouteService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@AllArgsConstructor
public class RouteController {

    private final RouteService routeService;

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/routing/{origin}/{destination}",
            produces = {"application/json"}
    )
    public ResponseEntity<Route> getRoute(@PathVariable String origin,
                                          @PathVariable String destination) throws RouteException {
        Route route = routeService.getRoute(origin, destination);
        return ResponseEntity.ok(route);
    }
}
