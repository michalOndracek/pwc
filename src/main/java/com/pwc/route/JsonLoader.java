package com.pwc.route;

import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.file.Files;

public class JsonLoader {
    public static String loadJsonFile(final String fileName) throws IOException {
        final var file = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + fileName);

        return new String(Files.readAllBytes(file.toPath()));
    }
}
