package com.pwc.route;

import com.pwc.route.exception.RouteException;
import com.pwc.route.model.Country;
import com.pwc.route.model.Route;
import com.pwc.route.service.CountryDataHolder;
import com.pwc.route.service.RouteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RouteServiceTest {

    public static final String CZE = "CZE";
    public static final String AUT = "AUT";
    public static final String DEU = "DEU";
    public static final String POL = "POL";
    public static final String SVK = "SVK";
    public static final String ITA = "ITA";
    public static final String USA = "USA";
    private RouteService routeService;

    @BeforeEach
    void setUp() {
        routeService = new RouteService(createMockedCountryDataHolder());
    }

    @Test
    void testGetRoute() throws RouteException {
        Route route = routeService.getRoute(CZE, ITA);
        List<String> expected = List.of(CZE, AUT, ITA);
        assertEquals(expected, route.getRoute());
    }

    @Test
    void testGetRouteInvalidOrigin() {
        RouteException exception = assertThrows(RouteException.class, () -> routeService.getRoute("ABC", ITA));
        assertEquals("Origin country code is not cca3", exception.getMessage());
    }

    @Test
    void testGetRouteInvalidDestination() {
        RouteException exception = assertThrows(RouteException.class, () -> routeService.getRoute(CZE, "XYZ"));
        assertEquals("Destination country code is not cca3", exception.getMessage());
    }

    @Test
    void testGetRouteNoRoute() {
        RouteException exception = assertThrows(RouteException.class, () -> routeService.getRoute(ITA, USA));
        assertEquals("There is no route between the countries", exception.getMessage());
    }

    private CountryDataHolder createMockedCountryDataHolder() {
        Country cze = new Country(CZE, Set.of(AUT, DEU, POL, SVK));
        Country deu = new Country(DEU, Set.of(AUT, CZE, POL));
        Country pol = new Country(POL, Set.of(CZE, DEU, SVK));
        Country svk = new Country(SVK, Set.of(AUT, POL, CZE));
        Country aut = new Country(AUT, Set.of(CZE, DEU, ITA, SVK));
        Country ita = new Country(ITA, Set.of(AUT));
        Country usa = new Country(USA, Set.of());
        return new CountryDataHolder(Map.of(CZE, cze, DEU, deu, SVK, svk, AUT, aut, ITA, ita, POL, pol, USA, usa));
    }

}
