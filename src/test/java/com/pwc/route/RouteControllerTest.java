package com.pwc.route;

import com.pwc.route.controller.RouteController;
import com.pwc.route.model.Route;
import com.pwc.route.service.RouteService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RouteController.class)
public class RouteControllerTest {

    private static final String CZE = "CZE";
    private static final String AUT = "AUT";
    private static final String ITA = "ITA";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RouteService routeService;

    @Test
    void getRouteTest() throws Exception {
        //GIVEN
        when(routeService.getRoute(CZE, ITA)).thenReturn(createRoute());

        // WHEN
        // THEN
        mvc.perform(get("/routing/" + CZE + "/" + ITA).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.route", is(List.of(CZE, AUT, ITA))));
    }

    private Route createRoute() {
        return new Route(List.of(CZE, AUT, ITA));
    }
}
